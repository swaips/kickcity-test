import React, { Component } from 'react';
import cn from 'classnames';

import styles from './Location.css';
import pointIcon from '../../assets/images/pointIcon.png';
import arrowDownIcon from '../../assets/images/arrowDownIcon.png';
import Dropdown from '../Dropdown/Dropdown';

const cities = [
  { link: '/', title: 'Moscow' },
  { link: '/', title: 'Saint-Petersburg' },
  { link: '/', title: 'Helsinki' },
  { link: '/', title: 'Totonto' },
  { link: '/', title: 'Dubai' },
];

class Location extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dropdownIsOpen: false,
    };
    this.clickHandler = this.clickHandler.bind(this);
  }
  clickHandler() {
    this.setState({ dropdownIsOpen: !this.state.dropdownIsOpen });
  }
  render() {
    const { dropdownIsOpen } = this.state;
    const dropdownClasses = cn(styles.dropdown, {
      [styles.dropdownIsOpen]: dropdownIsOpen,
    });
    const dropdownIconClasses = cn(styles.dropdownIcon, {
      [styles.dropdownIconRotated]: dropdownIsOpen,
    });

    return (
      <div className={styles.location} onClick={this.clickHandler}>
        <img src={pointIcon} className={styles.pointIcon} alt="" />
        <div className={styles.selectedCityContainer}>
          <span className={styles.selectedCity}>Houston</span>
          <div className={dropdownClasses}>
            <Dropdown items={cities} />
          </div>
        </div>
        <img src={arrowDownIcon} className={dropdownIconClasses} alt="" />
      </div>
    );
  }
}

export default Location;
