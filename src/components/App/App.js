import React from 'react';
import Location from '../Location/Location';
import Search from '../Search/Search';
import Link from '../Link/Link';
import Profile from '../Profile/Profile';
import Button from '../Button/Button';
import Logo from '../Logo/Logo';
import Header from '../Header/Header';
import Menu from '../Menu/Menu';
import MenuItem from '../MenuItem/MenuItem';
import GetStartedScreen from '../GetStartedScreen/GetStartedScreen';
import EventsList from '../EventsList/EventsList';
import EventPreview from '../EventPreview/EventPreview';
import Notification from '../Notification/Notification';
import styles from './App.css';
import cover1 from '../../assets/images/event1.jpg';
import cover2 from '../../assets/images/event2.jpg';
import cover3 from '../../assets/images/event3.jpg';
import cover4 from '../../assets/images/event4.png';
import cover5 from '../../assets/images/event5.png';
import cover6 from '../../assets/images/event6.png';

const Fragment = React.Fragment;

const categories = [
  { link: '/', title: 'Conerts' },
  { link: '/', title: 'Party' },
  { link: '/', title: 'Art' },
  { link: '/', title: 'Games' },
  { link: '/', title: 'Festivals' },
  { link: '/', title: 'Education' },
  { link: '/', title: 'Theatre' },
  { link: '/', title: 'Fashion' },
  { link: '/', title: 'Nightlife' },
  { link: '/', title: 'Sport' },
  { link: '/', title: 'Business' },
  { link: '/', title: 'Meetups' },
];

const App = () => (
  <Fragment>
    <Header>
      <div className={styles.LogoWrap}>
        <Logo />
      </div>
      <Location />
      <div className={styles.searchContainer}>
        <Menu>
          <MenuItem title="Categories" submenu={categories} />
          <MenuItem link="/" title="Explore" />
        </Menu>
        <Search />
      </div>
      <Notification />
      <Link className={styles.createEvent}>Create Event</Link>
      <Profile />
    </Header>
    <GetStartedScreen />
    <EventsList title="Trending events">
      <EventPreview
        title="New Politics @ House of Blues@ House of Blues"
        date="Feb, 15"
        cover={cover1}
      />
      <EventPreview
        title="Grand Budapest Hotel"
        date="Feb, 13"
        cover={cover2}
        hasCoin
        isFree
      />
      <EventPreview
        title="Romeo and Juliet"
        date="Apr, 24"
        cover={cover3}
        bigSize
      />
      <EventPreview
        title="A$AP ROCKY"
        date="Mar, 21"
        cover={cover4}
      />
      <EventPreview
        title="Dieter Rams: Less is Better"
        date="Apr, 07"
        cover={cover5}
        bigSize
        hasCoin
      />
      <EventPreview
        title="Carel Fabritius"
        date="Mar, 13—25"
        cover={cover6}
      />
    </EventsList>
  </Fragment>
);

export default App;
