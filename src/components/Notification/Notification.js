import React from 'react';
import Link from '../Link/Link';
import styles from './Notification.css';
import notificationIcon from '../../assets/images/notificationIcon.png';

const Notification = () => (
  <div className={styles.notification} >
    <Link className={styles.link} to="/">
      <img src={notificationIcon} alt="" />
      <div className={styles.alarm} />
    </Link>
  </div>
);

export default Notification;
