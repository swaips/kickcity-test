import React from 'react';
import styles from './MenuItem.css';
import Link from '../Link/Link';
import Dropdown from '../Dropdown/Dropdown';

const MenuItem = ({ title = '', link = '/', submenu = [] }) => (
  <li className={styles.menuItem}>
    <div className={styles.linkContainer}>
      <Link to={link} className={styles.link} >{title}</Link>
      {
        !!submenu.length &&
        <div className={styles.dropdown}>
          <Dropdown items={submenu} columns={2} />
        </div>
      }
    </div>
  </li>
);

export default MenuItem;
