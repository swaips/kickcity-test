import React, { Component } from 'react';
import cn from 'classnames';
import styles from './Search.css';
import searchIcon from '../../assets/images/searchIcon.png';
import closeIcon from '../../assets/images/close.png';

class Search extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isVisible: false,
      query: '',
    };
    this.toggleSearchVisible = this.toggleSearchVisible.bind(this);
  }

  toggleSearchVisible() {
    this.setState({ isVisible: !this.state.isVisible });
  }

  render() {
    const { isVisible, query } = this.state;
    const searchClasses = cn(styles.search, {
      [styles.searchIsVisible]: isVisible,
    });
    return (
      <div className={searchClasses} >
        {
          isVisible && <input
            placeholder="Example: Cats exhibition"
            type="text"
            value={query}
            className={styles.input}
          />
        }
        {isVisible && <button className={styles.submit} >Search</button> }
        <button className={styles.searchButton} onClick={this.toggleSearchVisible}>
          {
            isVisible ?
              <img src={closeIcon} alt="" /> :
              <img src={searchIcon} alt="" />
          }
        </button>
      </div>
    );
  }
}

export default Search;
