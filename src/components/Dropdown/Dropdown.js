import React from 'react';
import styles from './Dropdown.css';
import Link from '../Link/Link';

const Dropdown = ({ children, items = [], columns = 1 }) => (
  <div className={styles.dropdown}>
    <ul className={styles.list} style={{ columns }}>
      {
        items.map(item => <li key={item.title} className={styles.listItem}><Link to={item.link}>{item.title}</Link></li>)
      }
    </ul>
    {children}
  </div>
);

export default Dropdown;
