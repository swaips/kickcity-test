import React from 'react';
import cn from 'classnames';
import styles from './Button.css';

const Button = ({ children, type = 'primary' }) => {
  const buttonClasses = cn(styles.button, styles[type]);
  return <button className={buttonClasses}>{children}</button>;
};

export default Button;
