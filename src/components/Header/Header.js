import React from 'react';
import styles from './Header.css';

const Header = ({ children }) => (
  <header className={styles.header}>
    <div className={styles.container}>
      {children}
    </div>
  </header>
);

export default Header;
