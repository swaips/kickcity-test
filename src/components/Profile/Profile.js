import React, { Component } from 'react';
import cn from 'classnames';
import Link from '../Link/Link';
import Dropdown from '../Dropdown/Dropdown';

import styles from './Profile.css';
import avatar from '../../assets/images/avatar.png';

const menu = [
  { link: '/', title: 'My profile' },
  { link: '/', title: 'Organizer panel' },
  { link: '/', title: 'Buy KCY Tokens' },
  { link: '/', title: 'Settings' },
];

class Profile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dropdownIsOpen: false,
    };
    this.clickHandler = this.clickHandler.bind(this);
  }
  clickHandler() {
    this.setState({ dropdownIsOpen: !this.state.dropdownIsOpen });
  }
  render() {
    const { dropdownIsOpen } = this.state;
    const dropdownClasses = cn(styles.dropdown, {
      [styles.dropdownIsOpen]: dropdownIsOpen,
    });

    return (
      <div className={styles.profile} onClick={this.clickHandler}>
        <div className={styles.info}>
          <div className={styles.balance}>
            <div className={styles.value}>0.0027</div>
            <div className={styles.currency}>KCY</div>
          </div>
          <div className={dropdownClasses}>
            <Dropdown items={menu} >
              <hr className={styles.menuSeporator} />
              <Link to="/" >Log Out</Link>
            </Dropdown>
          </div>
        </div>

        <img className={styles.avatar} src={avatar} alt="" />
      </div>
    );
  }
}

export default Profile;
