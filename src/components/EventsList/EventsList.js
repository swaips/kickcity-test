import React from 'react';
import Link from '../Link/Link';

import styles from './EventsList.css';
import fire from '../../assets/images/fire.png';
import arrowRightIcon from '../../assets/images/arrowRightIcon.png';

const EventsList = ({ children, title = 'Events' }) => (
  <section className={styles.container}>
    <header className={styles.header}>
      <h2 className={styles.h}>
        {title}
        <img className={styles.headerIcon} src={fire} alt="" />
      </h2>
      <Link to="/">More <img className={styles.moreIcon} src={arrowRightIcon} alt="" /></Link>
    </header>
    <section className={styles.list}>
      {children}
    </section>
  </section>
);

export default EventsList;
