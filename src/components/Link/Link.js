import React from 'react';
import styles from './Link.css';

const Link = ({ children, to = '/', className = styles.link }) => <a className={className} href={to}>{children}</a>;

export default Link;
