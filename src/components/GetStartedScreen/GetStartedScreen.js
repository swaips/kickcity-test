import React from 'react';
import styles from './GetStartedScreen.css';
import Button from '../Button/Button';
import picture from '../../assets/images/illustration.png';

const GetStartedScreen = () => (
  <div className={styles.getStartedScreen}>
    <img src={picture} alt="" className={styles.picture} />
    <div className={styles.textContainer} >
      <h1 className={styles.h}>Get rewarded<br />for enjoying life!</h1>
      <p className={styles.p} >Earn crypto tokens by having fun with friends</p>
      <Button>Get Started</Button>
    </div>
  </div>
);

export default GetStartedScreen;
