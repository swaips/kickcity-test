import React from 'react';
import styles from './EventPreview.css';
import favoriteIcon from '../../assets/images/favoriteIcon.png';
import coin from '../../assets/images/coin.png';
import Link from '../Link/Link';
import sn from 'classnames';

const EventPreview = ({
  date = '',
  cover = '',
  title = '',
  bigSize = false,
  hasCoin = false,
  isFree = false,
}) => {
  const eventPreviewClasses = sn(styles.eventPreview, {
    [styles.eventPreviewBigSize]: bigSize,
  });
  return (
    <div className={eventPreviewClasses}>
      <div className={styles.coverContainer}>
        <img className={styles.cover} src={cover} alt="" />
      </div>
      <div className={styles.content}>
        <Link className={styles.link} />
        <div className={styles.contentHeader}>
          <div className={styles.date}>{date}</div>
          <div className={styles.contentBox}>
            {
              hasCoin &&
              <div className={styles.coinContainer}>
                <div className={styles.coin}>
                  <img className={styles.coinIcon} src={coin} alt="" />
                </div>
                <div className={styles.coinTooltip}>Visit this event and earn KCY tokens!</div>
              </div>
            }
            {
              isFree && <div className={styles.freeLabel}>Free</div>
            }
          </div>
        </div>
        <div className={styles.contentFooter}>
          <div className={styles.contentBox}>
            <h3 className={styles.h}>{title}</h3>
            <div className={styles.favorite}>
              <img src={favoriteIcon} alt="" />
            </div>
          </div>
          <div className={styles.tags}>
            <Link className={styles.tag}>Party</Link>
            <Link className={styles.tag}>Concert</Link>
          </div>
        </div>
      </div>
    </div>
  );
};

export default EventPreview;
