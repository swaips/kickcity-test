import React from 'react';
import logoImage from '../../assets/images/logo.png';
import Link from '../Link/Link';

const Logo = ({ link = '/', alt = '' }) => (
  <Link to={link} >
    <img src={logoImage} alt={alt} />
  </Link>
);

export default Logo;
