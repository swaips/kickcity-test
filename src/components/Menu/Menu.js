import React from 'react';
import styles from './Menu.css';

const Menu = ({ children }) => <ul className={styles.menu}>{children}</ul>;

export default Menu;
