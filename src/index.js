import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App/App';
import './assets/css/global.css';

document.addEventListener('DOMContentLoaded', () => {
  ReactDOM.render(<App />, document.getElementById('app'));
});
