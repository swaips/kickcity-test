const webpack = require('webpack');
const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

let config = {
    entry: './index.js',
    mode: 'development',
    context: path.join(__dirname, 'src'),
    output: {
        path: path.join(__dirname, 'dist'),
        publicPath: 'dist/',
        filename: 'bundle.js'
    },
    // plugins: [
    //     new ExtractTextPlugin({
    //         filename: 'css/style.css'
    //     })
    // ],
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: ['node_modules'],
                use: {
                    loader: "babel-loader",
                    options: { presets: ['es2015', 'react']}
                }
            },
            {
                test: /\.css$/,
                // use: ExtractTextPlugin.extract({
                //     fallback: 'style-loader',
                //     use: [
                //         'style-loader',
                //         {
                //             loader: 'css-loader', options: {
                //                 modules: true,
                //                 importLoaders: 1,
                //                 localIdentName: '[name]__[local]--[hash:base64:5]'
                //             }
                //         },
                //         'postcss-loader'
                //     ]
                // }),
                use: [
                    'style-loader',
                    {
                        loader: 'css-loader', options: {
                            modules: true,
                            importLoaders: 1,
                            localIdentName: '[name]__[local]--[hash:base64:5]'
                        }
                    },
                    'postcss-loader'
                ]
            },
            {
                test: /\.(jpg|png|svg)$/,
                use: {
                    loader: "file-loader",
                    options: {
                        name: 'assets/images/[name].[ext]'
                    }
                }
            },
            { 
                test: /\.(woff2?|ttf|eot|svg)$/, 
                loader: "file-loader" 
            },
        ]
    },
    devServer: {
        compress: true,
        open: true,
        port: 9000
    }
}

module.exports = config;